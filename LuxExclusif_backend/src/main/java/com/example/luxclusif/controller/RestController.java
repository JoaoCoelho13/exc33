package com.example.luxclusif.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.example.luxclusif.message.ResponseMessage;
import com.example.luxclusif.model.FileInfo;
import com.example.luxclusif.model.Product;
import com.example.luxclusif.service.FilesStorageService;
import com.example.luxclusif.service.GoogleSheetsService;
import com.example.luxclusif.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class RestController {

    @Autowired
    private FilesStorageService storageService;

    @Autowired
    private ProductService productService;

    @Autowired
    private GoogleSheetsService sheetsService;

    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadFiles(@RequestParam("files") MultipartFile[] files) {
        String message = "";
        try {
            List<String> fileNames = new ArrayList<>();

            Arrays.asList(files).stream().forEach(file -> {
                storageService.save(file);
                fileNames.add(file.getOriginalFilename());
            });

            message = "Uploaded the files successfully: " + fileNames;
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = e.getMessage();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }

    @PostMapping("/addProduct")
    public Product addProduct(@RequestBody Product product) {

        //meter o try catch no servico apagar souts
        try {
            sheetsService.save(product);

        } catch (IOException e) {

            System.out.println(e.getMessage());
        } catch (GeneralSecurityException e) {

            System.out.println(e.getMessage());
        }
        return productService.saveProduct(product);
    }


    @PutMapping("/update")
    public Product updateProduct(@RequestBody Product product) {
        return productService.updateProduct(product);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id) {
        return productService.deleteProduct(id);
    }

}

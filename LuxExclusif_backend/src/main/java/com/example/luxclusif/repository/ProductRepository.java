package com.example.luxclusif.repository;


import com.example.luxclusif.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    Product findByFirstName(String firstName);
}

package com.example.luxclusif.service;

import com.example.luxclusif.model.Product;
import com.example.luxclusif.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class ProductService {

    private List<MultipartFile> files;

    @Autowired
    private ProductRepository repository;

    public Product saveProduct(Product product) {
        return repository.save(product);
    }

    public List<Product> saveProducts(List<Product> products) {
        return repository.saveAll(products);
    }


    public List<Product> getProducts() {
        return repository.findAll();
    }

    public Product getProductById(int id) {
        return repository.findById(id).orElse(null);
    }

    public Product getProductByName(String firstName) {
        return repository.findByFirstName(firstName);
    }


    public String deleteProduct(int id) {
        repository.deleteById(id);
        return "product removed !! " + id;
    }

    public Product updateProduct(Product product) {
        Product existingProduct = repository.findById(product.getId()).orElse(null);
        existingProduct.setFirstName(product.getFirstName());
        existingProduct.setBrand(product.getBrand());

        return repository.save(existingProduct);
    }

    public void saveFile(MultipartFile file) {
        files.add(file);
    }
}

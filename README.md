To start the project:
    
    
    1. Open MySQL
        1.1 Create Database luxclusif
        1.2 Create user 'luxclusif'@'%' and grant all permissions on luxclusif.*

    2. Open the backend file on your IDE from the Pom.xml
        2.1 Run the app on the LaunchApplication class
        *(Possibly required to start your google drive and sheets api)

    3. Open the frontend file
        3.1 cd into the directory where src is contained and insert the command "npm start"

Test the app.


Note: Because of time restrictions and some bad decisions on time usage I was unable to complete the project on time.
import React from 'react';
import "./Footer.css";

function Footer() {
    return (
        <div class="footer section">
            <div class="container">
                <div class="row">
                    
                    <div class="baseline col-lg-4 col-md-12 col-sm-12 col-xs-12 pb30">
                        <div class="accordion-header">
                            <h5>
                                <span class="fas fa-plus show-icon joao1-style"></span>
                                Farfetch App
                            </h5>
                            <div class="accordion-content">
                                <div class="pt10">
                                    <a href="/" class="no-underline color-white primary" target="_blank">
                                        <img src="https://secondlife.farfetch.com/img/tablet.png" alt="tablet-mobile" class="pr20"/>
                                        <span class="discover-app-text color-white block">Farfetch App for iOS and Android</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="baseline col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="row pb30" id="country-region-1">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h5>
                                    Country/Region, Currency and Language
                                </h5>
                                <div>
                                    <a href="/" class="no-underline color-white primary pl10" target="_blank">
                                        <div class="ChangeCountry_Flag flags flag-gb f18"></div>
                                        <span class="countryregion color-white block pl10">United Kingdom, GBP £</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row pb30">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="accordion-header">
                                    <h5>
                                        <span class="fas fa-plus show-icon joao1-style"></span>
                                            Follow Us
                                    </h5>
                                    <div class="accordion-content">
                                        <div class="social-icons pt10">
                                            <div class="inlineBlock">
                                                <a href="/" target="_blank">
                                                    <img src="https://secondlife.farfetch.com/img/social-media-icons/instagram.png" alt="instagram" width="17" height="17"/>
                                                </a>
                                            </div>
                                            <div class="inlineBlock">
                                                <a href="https://www.facebook.com/farfetch.asiapac/?brand_redir=88573992939">
                                                    <img src="https://secondlife.farfetch.com/img/social-media-icons/facebook.png" alt="facebook" width="17" height="17"/>
                                                </a>
                                            </div>
                                            <div class="inlineBlock">
                                                <a href="https://www.twitter.com/farfetch">
                                                <img src="https://secondlife.farfetch.com/img/social-media-icons/twitter.png" alt="twitter" width="17" height="17"/>
                                                </a>
                                            </div>
                                            <div class="inlineBlock">
                                                <a href="https://www.snapchat.com/add/farfetch.com">
                                                    <img src="https://secondlife.farfetch.com/img/social-media-icons/snapchat.png" alt="snapchat" width="17" height="17"/>
                                                </a>
                                            </div>
                                            <div class="inlineBlock">
                                                <a href="https://www.pinterest.com/farfetch">
                                                    <img src="https://secondlife.farfetch.com/img/social-media-icons/pinterest.png" alt="pinterest" width="17" height="17"/>
                                                </a>
                                            </div>
                                            <div class="inlineBlock">
                                                <a href="https://www.youtube.com/farfetch">
                                                    <img src="https://secondlife.farfetch.com/img/social-media-icons/youtube.png" alt="youtube" width="22" height="17"/>
                                                </a>
                                            </div>
                                            <div class="inlineBlock">
                                                <a href="https://plus.google.com/109435701726892308651?prsrc=3">
                                                <img src="https://secondlife.farfetch.com/img/social-media-icons/google-plus.png" alt="google-plus" width="26" height="17"/>
                                                </a>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class="baseline col-lg-2 col-md-12 col-sm-12 col-xs-12 pb30">
                        <div class="accordion-header">
                            <h5>
                                <span class="fas fa-plus show-icon joao1-style"></span>
                                    Customer Service
                            </h5>
                            <div class="accordion-content">
                                <div class="pt10">
                                    <ul class="list-regular color-white no-underline">
                                        <li>
                                            <a class="no-underline color-white" href="/" target="_blank">Help</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" href="/">Contact Us</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" href="/" target="_blank">FAQs</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" href="/" target="_blank">Terms &amp; Conditions</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" href="/" target="_blank">Privacy Policy</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" href="/" target="_blank" data-toggle="modal" data-target="#cookiesModal">Cookie Preferences</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="baseline col-lg-2 col-md-12 col-sm-12 col-xs-12">
                        <div class="accordion-header">
                            <h5>
                                <span class="fas fa-plus show-icon joao1-style"></span>
                                About Farfetch
                            </h5>
                            <div class="accordion-content">
                                <div class="pt10">
                                    <ul class="list-regular color-white no-underline">
                                        <li>
                                            <a class="no-underline color-white" target="_blank" href="/">About Us</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" target="_blank" href="/">Investors</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" target="_blank" href="/">Farfetch&nbsp;Boutique&nbsp;Partners</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" target="_blank" href="/">Affiliate Programme</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" target="_blank" href="/">Careers</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" target="_blank" href="/">Farfetch&nbsp;Customer&nbsp;Promise</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" target="_blank" href="/">Farfetch App</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" target="_blank" href="/">Farfetch Reviews</a>
                                        </li>
                                        <li>
                                            <a class="no-underline color-white" target="_blank" href="/">Sitemap</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row pt60">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 baseline h6">
                        <span class="footer-disclaimer color-medium-grey">
                            'farfetch' and the 'farfetch' logo are trade marks of Farfetch UK Limited and are registered in numerous jurisdictions around the world.
                        </span>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 baseline h6">
                        <span class="footer-disclaimer color-medium-grey">
                            © Copyright 2019 Farfetch UK Limited. All rights reserved.
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer

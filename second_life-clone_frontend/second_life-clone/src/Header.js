import React from 'react';
import "./Header.css";
import { Link } from "react-router-dom";

function Header() {
    return (
        <div class="ff-header d-flex justify-content-center">
            <Link to="/">
                <div class="home home-big d-none d-sm-flex">
                    <img 
                        class="ff-logo align-self-center" 
                        alt="Farfetch Secondlife"
                        src="https://secondlife.farfetch.xom/img/logo/logo-horizontal.svg"
                    />
                </div>
            </Link>
            

            <ul class="ff-header-help d-flex align-items-center">
                <li class="d-none d-md-block">
                    <a href="/">NEED HELP?</a>
                </li>
                <li>
                    
                    <a title="Farfetch Customer Service" href="tel: +351 925344934">
                        <img alt="tel-icon"
                            class="img__phone" 
                            src="https://secondlife.farfetch.com/img/phone.svg"
                        />

                    </a>
                </li>
            </ul>
            
        </div>
    )
}

export default Header

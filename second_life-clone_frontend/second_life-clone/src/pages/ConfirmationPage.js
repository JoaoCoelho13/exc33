import React from 'react';
import "./ConfirmationPage.css";
import { Link } from "react-router-dom";

class ConfirmationPage extends React.Component {
    render() {


        return (
            <div>
                <h4>Congratulations {this.props.user.firstName} {this.props.user.lastName} your transaction has been completed successfully!
                </h4>
                <p>
                    Continue Selling?
                </p>
                
                <div>
                    <Link to="/">
                        <button>Back to Homepage</button>
                    </Link>
                </div>

            </div>
        )
    }
}

export default ConfirmationPage

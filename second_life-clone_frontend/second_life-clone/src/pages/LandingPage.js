import React from 'react';
import "./LandingPage.css";
import { Link } from "react-router-dom";

function LandingPage() {

    return (
        <div className="section">
            <div class="heromask2">
                <div class="d-flex p-2 my-3 justify-content-center we-are-here joao-style">
                    <p class="m-0">
                        <span class="font-p-bold">We are here for you</span>
                        <span class="px-1">|</span>
                    Farfetch Second Life is continuing to pick up your items safely from home
                </p>
                </div>
                <picture>
                    <source type="image/webp"
                        srcset="https://secondlife.farfetch.com/img/banners/ff-desktop.webp"
                    />
                    <source type="image/jpeg"
                        srcset="https://secondlife.farfetch.com/img/banners/ff-desktop.jpg"
                    />
                    <img class=" " src="https://secondlife.farfetch.com/img/banners/ff-desktop.jpg"
                        alt="September Banner"
                    />
                </picture>
            </div>

            <div id="welcome-sell" class="container ff-section ff-section-top joao2-style">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="intro ff-title">
                            Sell your designer bags for Farfetch credit
                        </p>
                        <p class="joao3-style">
                            Introducing Farfetch Second Life, a new service to enable you to make space in your wardrobe by selling your bags
                        <br></br>
                        in exchange for credits to spend on Farfetch. Give your bags a second lease of life. Earn credit.*
                        </p>
                        <div class="joao4-style">
                            *Available in the UK, European Union and Norway. Please see
                        <a href="/">FAQs</a>
                        for more details.
                        <br></br>
                        Based elsewhere?
                        <a href="/">Get notified</a>
                        when Second Life launches near you.
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 ff-title p-0 mt-4">
                    <Link to="/sell">
                        <button id="ff-selling-up" class="btn btn-black ff-cta">
                        Start selling
                    </button>
                    </Link>
                </div>
            </div>

            <div class="container ff-section">
                <div class="row ff-title">
                    <div class="col-lg-12">
                        <p class="howitworks">
                            <strong>Here's how it works...</strong>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <center class="ff-title">
                            <img alt="how-it-works-1"
                                src="https://secondlife.farfetch.com/img/how-it-works/1.png"
                                class="joao5-style" />
                        </center>
                        <p class="steps ff-title">Get your quote</p>
                        <center>
                            <p class="substeps1 ff-title">
                                Upload photos of the bag you want to sell from the brands listed below, and within 2 business days we'll tell you how much credit you could earn
                            </p>
                        </center>
                    </div>
                    <br></br>
                    <div class="col-lg-4">
                        <center class="ff-title">
                            <img alt="how-it-works-2" src="https://secondlife.farfetch.com/img/how-it-works/2.png" class="joao5-style"></img>
                        </center>
                        <p class="steps ff-title">Arrange a free collection</p>
                        <center>
                            <p class="substeps2 ff-title">
                                Schedule a pick-up at a time that suits you, then we’ll take care of the rest
                            </p>
                        </center>
                    </div>
                    <div class="col-lg-4">
                        <center class="ff-title">
                            <img alt="how-it-works-3" src="https://secondlife.farfetch.com/img/how-it-works/3.png"
                                class="joao5-style" />
                        </center>
                        <p class="steps ff-title">Earn Farfetch credit</p>
                        <center>
                            <p class="substeps3 ff-title">
                                Get credit in just two business days once your bag has been received and verified at our warehouse
                            </p>
                        </center>
                    </div>
                </div>
            </div>

            <section class="ff-section whats-section">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="row-1 ff-title">
                                <div id="img-div" class="mask">
                                    <div class="bg-webp-img has-no-webp"></div>
                                    <center>
                                        <p class="whatsin joao6-style">
                                            What's in it for me?
                                        </p>
                                    </center>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="row-1 newstuff">
                                <div class="card joao7-style">
                                    <p class="steps ff-title">Instant payment</p>
                                    <center>
                                        <p class="substeps1 ff-section">
                                            We give you credit as soon as your bag has been<br class="d-none d-lg-block"></br>
                                                verified so you can fund your next purchase faster
                                        </p>
                                    </center>
                                    <p class="steps ff-title">Get something new
                                    </p>
                                    <center>
                                        <p class="substeps1 ff-section">
                                            Use your credit to buy new things on Farfetch
                                        </p>
                                    </center>
                                    <p class="steps ff-title">No fees</p>
                                    <center>
                                        <p class="substeps1 ff-section">
                                            The price we give you is the value of the credit you will get.<br class="d-none d-lg-block"></br>
                                            No hidden fees.
                                        </p>
                                    </center>
                                    <p class="steps ff-title">Do good</p>
                                    <center>
                                        <p class="substeps1 ff-section">
                                            By selling your bag, you’re doing your bit to extend<br class="d-none d-lg-block"></br>
                                            its life and help the environment
                                        </p>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <div class="row-1 ff-title">
                    <center><p class="welcome">At the moment we accept the following brands</p></center>
                </div>
            </div>

            <div class="container ff-title brands-container joao8-style">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <p class="brands hd-sml">Alexander McQueen</p>
                        <p class="brands hd-lrg">A.McQueen</p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <p class="brands hd-sml">Alexander Wang</p>
                        <p class="brands hd-lrg">A.Wang</p>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Balenciaga</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Bottega Veneta</p>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Burberry</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Bvlgari</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Cartier</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Celine</p>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Chanel</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Dior</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Dolce &amp; Gabbana</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Fendi</p>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Givenchy</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Gucci</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Hermès</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Jacquemus</p>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Loewe</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Louis Vuitton</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> MCM</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Miu Miu</p>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Mulberry</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Off White</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Prada</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Salvatore Ferragamo</p>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Stella McCartney</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> The Row</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Valentino</p>
                        </center>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> Versace</p>
                        </center>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                        <center>
                            <p class="brands"> YSL</p>
                        </center>
                    </div>
                </div>
                <p class="text-center joao9-style">Watch this space though, we're working on a broader initiative to enable you  to trade in other items
                </p>
            </div>

            <div class="container ff-section">
                <div class="center">
                    <p class="ff-h1">
                        See what previous customers have sold
                    </p>
                </div>

                <div class="row ff-customer-bags">
                    <div class="col p-2">
    
                        <img alt="AltText" 
                        src="https://secondlife.farfetch.com/img/pricing_skus/GUCCI.png" 
                        srcset="https://secondlife.farfetch.com/img/pricing_skus/GUCCI-small.png 390w, 
                        https://secondlife.farfetch.com/img/pricing_skus/GUCCI.png 400w" class="img-responsive"/>
                        <p class="ff-cb-brand mt-3">Gucci</p>
                        <p class="ff-cb-name">Large Leather Soho Tote</p>
                        <p class="ff-cb-price mt-1">£265</p>
                        <p class="ff-rating">Condition: Good</p>
                    </div>

                    <div class="col p-2">
                        <img alt="AltText" src="https://secondlife.farfetch.com/img/pricing_skus/LV.png" srcset="https://secondlife.farfetch.com/img/pricing_skus/LV-small.png 390w, 
                        https://secondlife.farfetch.com/img/pricing_skus/LV.png 400w" class="img-responsive" loading="lazy"/>
                        <p class="ff-cb-brand mt-3">Louis Vuitton</p>
                        <p class="ff-cb-name">Damier Ebene Trevi PM</p>
                        <p class="ff-cb-price mt-1">£485</p>
                        <p class="ff-rating">Condition: Good</p>
                    </div>

                    <div class="col p-2">
                        <img alt="AltText" src="https://secondlife.farfetch.com/img/pricing_skus/CHLOE.png" srcset="https://secondlife.farfetch.com/img/pricing_skus/CHLOE-small.png 390w, 
                        https://secondlife.farfetch.com/img/pricing_skus/CHLOE.png 400w" class="img-responsive" loading="lazy"/>
                        <p class="ff-cb-brand mt-3">Chloe</p>
                        <p class="ff-cb-name">Medium Leather Aby</p>
                        <p class="ff-cb-price mt-1">£805</p>
                        <p class="ff-rating">Condition: Excellent</p>
                    </div>


                    <div class="col p-2">
                        <img alt="AltText" src="https://secondlife.farfetch.com/img/pricing_skus/DIOR.png" srcset="https://secondlife.farfetch.com/img/pricing_skus/DIOR-small.png 390w, 
                        https://secondlife.farfetch.com/img/pricing_skus/DIOR.png 400w" class="img-responsive" loading="lazy"/>
                        <p class="ff-cb-brand mt-3">Dior</p>
                        <p class="ff-cb-name">Embroidered Book Tote</p>
                        <p class="ff-cb-price mt-1">£1,440</p>
                        <p class="ff-rating">Condition: Pristine</p>
                    </div>
                </div>
            </div>

            <div class="container joao10-style">
                <center>
                    <p class="ff-section joao11-style">
                        Please note that the price that you will get for your bag will depend on its condition - the better the condition, the better the price.
                        <br></br>
                        We only accept items that meet our condition criteria
                    </p>
                </center>
                <br></br>
            </div>

            <div class="container ff-section joao8-style">
                <center>
                    <p class="ff-title joao12-style">Got a bag to sell?</p>
                    <div class="col-lg-12 ff-title p-0">
                        <Link to="/sell">
                            <button id="ff-selling-started" class="btn btn-black ff-cta">
                            Start selling
                            </button>
                        </Link>
                        
                    </div>
                </center>
            </div>

            <div class="container ff-section joao13-style">
                <div class="center">
                    <p class="ff-h1">What our customers say about their Second Life experience...</p>
                
                    <div class="row pt-3">
                        <div class="col-md-6 col-sm-12 p-2">
                            <p class="font-italic">
                                <span class="s2-bold">“</span> I think the overall process was 
                                <span class="polaris-bold">super easy, simple and well-laid out</span>.
                                <span class="s2-bold">”</span>
                            </p>
                            <p class="p-author">by Manal W.</p>
                        </div>
                        <div class="col-md-6 col-sm-12 p-2">
                            <p class="font-italic">
                                <span class="s2-bold">“</span> Everything went smoothly as explained in the mail. <span class="polaris-bold">So easy, so fast.</span> Thank you!<span class="s2-bold"> ”</span>
                            </p>
                            <p class="p-author">by Deniz G.</p>
                        </div>
                        <div class="col-md-6 col-sm-12 offset-md-3 p-2">
                            <p class="font-italic">
                                <span class="s2-bold">“</span> The service was <span class="polaris-bold">informative and efficient</span>. I will definitely be using this service and recommending to friends and family <span class="s2-bold">”</span>
                            </p>
                            <p class="p-author">by Iram R.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container ff-section joao8-style">
                <center>
                    <p class="joao14-style">
                        <br></br>
                        We're on a mission to become the global platform for good in luxury fashion – empowering everyone to think, act and choose positively. Services like Farfetch Second Life help our customers extend the life of the clothes they buy. It's all part of what we call Positively Farfetch.
                    </p>
                    <p class="joao15-style">
                        <br></br>
                        <a href="/" title="Positively Farfetch">
                            <img src="https://secondlife.farfetch.com/img/logo/logo-vertical-positively.svg" alt="Positively Farfetch" class="positively img-fluid"/>
                        </a>
                    </p>
                </center>
            </div>
        
        </div>

    )
}

export default LandingPage;

import React, { Component } from 'react';
import "./SellingPage.css";
import axios from "axios";
import ConfirmationPage from "./ConfirmationPage";

class Sell extends Component {

    constructor(props) {
        super(props)

        this.state = {
            firstName: "",
            lastName: "",
            email: "",
            country: "",
            phone: "",
            brand: "",
            currentCondition: "",
            size: "",
            extras: "",
            selectedFile: [],
            transactionMade: false
        }

        this.submitHandler = this.submitHandler.bind(this);
    }

    changeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    fileSelectedHandler = event => {
        this.setState({
            selectedFile: event.target.files
        })
    }

    submitHandler = e => {
        e.preventDefault();

        axios.post('http://localhost:9191/addProduct', {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            country: this.state.country,
            phone: this.state.phone,
            currentCondition: this.state.currentCondition,
            brand: this.state.brand,
            size: this.state.size,
            extras: this.state.extras
        })
            .then(response => {
                console.log(response);

                const fd = new FormData();

                for (let i = 0; i < this.state.selectedFile.length; i++) {
                    fd.append('files', this.state.selectedFile.item(i));
                }

                axios.post('http://localhost:9191/upload', fd)
                    .then(res => {
                        console.log(res);
                        this.setState({
                            transactionMade : true
                        })
                        
                    })
                    .catch(error => {
                        console.log(error);
                    })
            })
            .catch(error => {
                console.log(error)
            })

    }

    render() {
        const { transactionMade, firstName, lastName, email, country, phone, currentCondition, brand, size, extras } = this.state;

        var user = {
            firstName: firstName,
            lastName: lastName
        }
        return (
            <div>

            {
                !transactionMade? (
                    <div>
                <section>
                    <div class="heromask joao1-style" id="#top">
                        <div class="banner-text">Start Selling</div>
                    </div>
                    <div id="ff-sell-form-x22" class="container">
                        <form
                            id="contact_form"
                            class="form-horizontal"
                            onSubmit={this.submitHandler}>
                            <fieldset>
                                <div class="row-1">
                                    <div class="col-lg-12">
                                        <div class="center">
                                            <p class="joao2-style">
                                                Tell us about your bag and we will tell you how much credit you could earn.
                                            </p>
                                            <p class="joao-3-style">
                                                Before submitting your application please
                                                <a class="p-guide" href="/" data-toggle="modal" data-target="#photoGuidelineModal">review our photo guidelines</a> and note that this service is currently only available to customers in the UK and select European Union Countries. Please see FAQs for more details.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="instruction">
                                    <div id="userForm" class="row">
                                        <div class="col-lg-6 ff-section">
                                            <label class="ff-title polar-bold"
                                                for="firstName">First Name
                                            </label>
                                            <input
                                                id="firstName"
                                                class="form-control text-form p-2"
                                                type="text"
                                                name="firstName"
                                                value={firstName}
                                                required
                                                onChange={this.changeHandler}
                                                />
                                        </div>
                                        <div class="col-lg-6 ff-section">
                                            <label class="ff-title polar-bold"
                                                for="lastName">Last Name</label>
                                            <input
                                                id="lastName"
                                                class="form-control text-form p-2"
                                                type="text"
                                                name="lastName"
                                                value={lastName}
                                                required
                                                onChange={this.changeHandler}
                                                />
                                        </div>
                                        <div class="col-lg-6 ff-section">
                                            <label class="ff-title polar-bold"
                                                for="email">Email Address</label>
                                            <input
                                                id="email"
                                                class="form-control text-form p-2"
                                                type="email"
                                                name="email"
                                                value={email}
                                                required
                                                onChange={this.changeHandler}
                                                />
                                            <span class="messages p-2"></span>
                                        </div>
                                        <div class="col-lg-6 ff-section ff-country">
                                            <label for="country" class="ff-title polar-bold">Country</label>
                                            <input
                                                id="country"
                                                class="form-control text-form p-2"
                                                type="text"
                                                name="country"
                                                value={country}
                                                required
                                                onChange={this.changeHandler}
                                                />
                                        </div>
                                        <div class="col-lg-6 ff-section">
                                            <label class="ff-title polar-bold"
                                                for="phone">Phone Number</label>
                                            <input
                                                id="phone"
                                                class="form-control text-form p-2"
                                                type="text"
                                                name="phone"
                                                value={phone}
                                                required
                                                onChange={this.changeHandler}
                                                />
                                        </div>
                                        <div class="col-lg-6 ff-section">
                                            <label class="ff-title polar-bold"
                                                for="currentCondition">Condition</label>
                                            <input
                                                id="currentCondition"
                                                class="form-control text-form p-2"
                                                type="text"
                                                name="currentCondition"
                                                value={currentCondition}
                                                required
                                                onChange={this.changeHandler}
                                                />
                                        </div>
                                        <div class="col-lg-6 ff-section">
                                            <label class="ff-title polar-bold"
                                                for="brand">Brand</label>
                                            <input
                                                id="brand"
                                                class="form-control text-form p-2"
                                                type="text"
                                                name="brand"
                                                value={brand}
                                                required
                                                onChange={this.changeHandler}
                                                />
                                        </div>
                                        <div class="col-lg-6 ff-section">
                                            <label class="ff-title polar-bold"
                                                for="size">Size</label>
                                            <input
                                                id="size"
                                                class="form-control text-form p-2"
                                                type="text"
                                                name="size"
                                                value={size}
                                                required
                                                onChange={this.changeHandler}
                                                />
                                        </div>
                                        <div class="col-lg-6 ff-section">
                                            <label class="ff-title polar-bold"
                                                for="extras">Extras</label>
                                            <input
                                                id="extras"
                                                class="form-control text-form p-2"
                                                type="text"
                                                name="extras"
                                                value={extras}
                                                required
                                                onChange={this.changeHandler}
                                                />
                                        </div>
                                        <label class="ff-title polar-bold"
                                            for="images">Upload at least 3 images of your product 
                                            
                                            </label>
                                            <h1> </h1>
                                        <div>
                                            <input
                                                id="images"
                                                type="file"
                                                placeholder="Upload Images"
                                                multiple
                                                onChange={this.fileSelectedHandler}
                                                />
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit">Submit</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                   
                </section>
            </div>
            ) : (
            <ConfirmationPage user={user}/>
            )}
            </div>
            
        )
    }

}

export default Sell;
